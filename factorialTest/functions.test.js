const factorialNumber = require('../fuctions.js');
var assert = require('assert');
describe('Factorial', function() {
  it('factorial 6 = 720', function() {
    const mathsService = new factorialNumber();
    assert.equal(mathsService.factorial(6), 720);
  });
  it('factorial 0 = 1', function() {
    const mathsService = new factorialNumber();
    assert.equal(mathsService.factorial(0), 1);
  });
  it('factorial -5 = -120', function() {
    const mathsService = new factorialNumber();
    assert.equal(mathsService.factorial(-5), -120);
  });
});