const mongoose         = require('mongoose');
const validator        = require('validator');
const User = mongoose.model("User", {
  name    : {
    type    : String,
    required: true,
    trim    : true,
    validate(value){
      if(value.length < 2){
        throw new Error("Sorry, but username must contain at least two symbols");
      }
    }
  },
  age     : {
    type: Number,
    validate(value) {
      if (value < 0) {
        throw new Error("Sorry, but age can't be below 0");
      }
    }
  },
  email   : {
    type  : String,
    unique: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("It's not an email")
      }
    }
  }
});
module.exports = User;