let User = require('../models/userModel');
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();
chai.use(chaiHttp);

describe('Insert Users', () => {
  before((done) => {
    User.deleteMany({}, (err) => {
      done();
    });
  });
  describe('#Save', ()=>{
    var users = [
      {name: "Jane", email: "jane@gmail.com", age: 10},
      {name: "John Cena", email: "cena@gmail.com", age: -43},
      {name: "Dave Mustaine", email: "mustaine@gmail.com", age: 58},
      {name: "James Hetfield", email: "mustaine@gmail.com", age: 54},
      {name: "B", email: "bobby@gmail.com", age: 18},
      {name: "Rocky Balboa", email: "balboa@gmail.com", age: -90},
      {name: "D", email: "denny@gmail.com", age: 48},
      {name: "NotAMan", email: "jane@gmail.com", age: 18},
      {name: "Kirk Hammett", email: "hammet@gmail.com", age: 57}
    ];
    users.forEach(function (user, index) {
      it(`User ${index} is testing...`, (done) => {
        chai.request(app)
          .post('/users/add')
          .send(user)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    })
  });
});