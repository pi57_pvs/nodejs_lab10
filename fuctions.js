module.exports = class factorialCreate {
  factorial(num) {
    let minusNum = num;
    if (num < 0) {
      minusNum = minusNum * -1;
      return (minusNum * this.factorial(minusNum - 1) * -1);
    }
   else if (num === 0)
      return 1;
    else {
      return (num * this.factorial(num - 1));
    }
  }
};