var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require("../models/userModel");


mongoose.connect('mongodb://localhost:27017',{
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});


router.post('/add',  function(req, res, next) {
    let user = req.body;
    let userToDb               = new User(user);
    userToDb.save(function (err) {
      if(err){
        res.status(403).send();
      }else{
        res.status(200).send();
      }
    });
});

module.exports = router;
